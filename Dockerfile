FROM osrf/ros:kinetic-desktop-full

ENV NVIDIA_VISIBLE_DEVICES \
    ${NVIDIA_VISIBLE_DEVICES:-all}

ENV NVIDIA_DRIVER_CAPABILITIES \
    ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y apt-utils build-essential psmisc vim-gtk
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN apt-get update && apt-get install -q -y python-catkin-tools
RUN apt-get update && apt-get install -q -y ros-kinetic-hector-gazebo-plugins wget python-rosdep

# Install Gazebo

RUN apt-key update

RUN sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list' \
 && wget http://packages.osrfoundation.org/gazebo.key -O - | apt-key add - \
 && apt-get update

# Install required Python packages for analysis

RUN \
  apt-get install -y python-pip && \
  pip install --upgrade pip && \
  pip install matplotlib==2.0.2 && \
  pip install numpy && \
  pip install scipy && \
  pip install jupyter && \
  pip install seaborn && \
  pip install pandas && \
  pip install bokeh && \
  pip install rosbag_pandas
