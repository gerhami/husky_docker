#!/bin/bash
set -e

source /opt/ros/kinetic/setup.bash
source /husky_workspace/install/setup.bash

exec "$@"
